//
//  UrlRequest.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 30.10.2023.
//

import Foundation

 public func getRocketInfoJSON(completion: @escaping ([RocketData]?) -> Void) {
    
    let appiString = "https://api.spacexdata.com/v4/rockets"
    
    guard let url = URL(string: appiString) else {return}
    
    URLSession.shared.dataTask(with: url) { data, response, error in
        if let error = error {
            print(error)
            return
        }
        guard let data = data else { return }
        
        do {
            let rocketDate = try JSONDecoder().decode([RocketData].self, from: data)
            completion(rocketDate)
        } catch {
            print(error)
            completion(nil)
        }
    }.resume()
}

public func getLaunchDataJSON(completion: @escaping ([LaunchData]?) -> Void) {
    
    let appiString = "https://api.spacexdata.com/v4/launches"
    
    guard let url = URL(string: appiString) else { return }
    
    URLSession.shared.dataTask(with: url) { data, response, error in
        if let error = error {
            print(error)
            return
        }
        guard let data = data else { return }
        
        do {
            let launchData = try JSONDecoder().decode([LaunchData].self, from: data)
            completion(launchData)
                
        } catch {
            print(error)
            completion(nil)
        }
    }.resume()
}


