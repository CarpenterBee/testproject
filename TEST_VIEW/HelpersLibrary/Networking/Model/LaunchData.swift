//
//  LaunchDataAPI.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 02.11.2023.
//

import Foundation

public struct LaunchData: Codable {
    let success: Bool?
    let name: String
    let dateUTC: String
    let upcoming: Bool
    let rocket: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case name
        case dateUTC = "date_utc"
        case upcoming
        case rocket
    }
}
