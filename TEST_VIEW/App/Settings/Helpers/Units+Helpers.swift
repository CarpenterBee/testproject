//
//  Units+Helpers.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 17.11.2023.
//

import Foundation

public enum UnitType {
    case height
    case weight
}

