//
//  SettingsViewController.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 06.10.2023.
//

import UIKit

public final class SettingsViewController: UIViewController {
    private let closeButton = UIButton()
    private let titleLabel = UILabel()
    private let sigmentStackView = UIStackView()
    private let segmentContener = UIStackView()
    
    private let heightSegment = UnitSegment(type: .height)
    private let diameterSegment = UnitSegment(type: .height)
    private let massSegment = UnitSegment(type: .weight)
    private let payloadSegment =  UnitSegment(type: .weight)
    
    private var selectedSegments = UnitSelectedStatesModel()
    
    weak var RreloadCollectionreloadCollection: ReloadCollection?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    public func getIndexies() -> UnitSelectedStatesModel {
        return selectedSegments
    }
    
    private func setupUI() {
        view.backgroundColor = .black

        setupCloseButton()
        setupTitleLabel()
        setupSigmentStackView()
        setupNameSegment()
        setupUnitsInfo()
        updateSegment()
    }
    
    private func updateSegment() {
        if let segmentData = UserDefaults.standard.object(forKey: "segmentData") as? Data {
            let decoder = JSONDecoder()
            if let loadedSegmentData = try? decoder.decode(UnitSelectedStatesModel.self, from: segmentData){
                heightSegment.setup(index: (loadedSegmentData.height) ? 1:0)
                diameterSegment.setup(index: loadedSegmentData.diameter ? 1:0)
                massSegment.setup(index: loadedSegmentData.mass ? 1:0)
                payloadSegment.setup(index: loadedSegmentData.payload ? 1:0)
            }
        }
    }
    
    private func setupTitleLabel() {
        titleLabel.text = "Настройки"
        titleLabel.backgroundColor = .black
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.font = .systemFont(ofSize: 18, weight: .bold)
        view.addSubview(titleLabel)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            titleLabel.widthAnchor.constraint(equalToConstant: 140),
        ])
    }
    
    private func setupCloseButton() {
        closeButton.backgroundColor = .black
        closeButton.setTitle("Зaкрыть", for: .normal)
        closeButton.addTarget(self, action: #selector(closeScreen), for: .touchUpInside)
        
        view.addSubview(closeButton)
        
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            closeButton.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -10),
            closeButton.heightAnchor.constraint(equalToConstant: 30),
            closeButton.widthAnchor.constraint(equalToConstant: 100),
        ])
    }
    
    @objc func closeScreen() {
        saveState()
        self.dismiss(animated: true)
        RreloadCollectionreloadCollection?.update()
    }
    
    private func setupSigmentStackView() {
        sigmentStackView.backgroundColor = .black
        sigmentStackView.axis = .vertical
        sigmentStackView.distribution = .fillEqually
        sigmentStackView.spacing = 35
        
        view.addSubview(sigmentStackView)
        sigmentStackView.addArrangedSubview(heightSegment)
        sigmentStackView.addArrangedSubview(diameterSegment)
        sigmentStackView.addArrangedSubview(massSegment)
        sigmentStackView.addArrangedSubview(payloadSegment)
        
        
        sigmentStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sigmentStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            sigmentStackView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -30),
            sigmentStackView.heightAnchor.constraint(equalToConstant: 260),
            sigmentStackView.widthAnchor.constraint(equalToConstant: 120),
        ])
    }
        
    private func saveState() {
        selectedSegments.diameter = diameterSegment.getIndex() != 0
        selectedSegments.height = heightSegment.getIndex() != 0
        selectedSegments.mass = massSegment.getIndex() != 0
        selectedSegments.payload = payloadSegment.getIndex() != 0

        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(selectedSegments) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "segmentData")
        }
    }
    
    private func setupNameSegment() {
        segmentContener.backgroundColor = .black
        view.addSubview(segmentContener)
        
        segmentContener.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            segmentContener.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            segmentContener.rightAnchor.constraint(equalTo: sigmentStackView.leftAnchor),
            segmentContener.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 30),
            segmentContener.heightAnchor.constraint(equalToConstant: 260),
        ])
    }
    
    private func setupUnitsInfo() {
        segmentContener.axis = .vertical
        segmentContener.spacing = 35
        
        ["Высота", "Диаметр", "Масса", "Полезная нагрузка" ].forEach { [weak self] text in
            let label = UILabel()
            label.text = text
            label.textColor = .white
            label.textAlignment = .left
            label.font = .systemFont(ofSize: 18)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.heightAnchor.constraint(equalToConstant: 38.75),
                label.widthAnchor.constraint(equalToConstant: 120)
                ])
            self?.segmentContener.addArrangedSubview(label)
        }
    }
}
