//
//  SigentModel.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 23.10.2023.
//

import Foundation
import UIKit

final class UnitSegment: UIView {
    private var type: UnitType
    private let segmentView = UISegmentedControl()

    public init(type: UnitType = .height) {
        self.type = type
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func getIndex() -> Int {
        return segmentView.selectedSegmentIndex
    }
    
    public func setup(index: Int) {
        segmentView.selectedSegmentIndex = index
    }
    
    private func setupUI() {
        switch type {
        case .height:
            segmentView.insertSegment(withTitle: "m", at: 0, animated: true)
            segmentView.insertSegment(withTitle: "ft", at: 1, animated: true)
        case .weight:
            segmentView.insertSegment(withTitle: "kg", at: 0, animated: true)
            segmentView.insertSegment(withTitle: "lb", at: 1, animated: true)
        }
        segmentView.backgroundColor = .gray
        segmentView.selectedSegmentIndex = 0
        segmentView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(segmentView)
        NSLayoutConstraint.activate([
            segmentView.topAnchor.constraint(equalTo: topAnchor),
            segmentView.leftAnchor.constraint(equalTo: leftAnchor),
            segmentView.rightAnchor.constraint(equalTo: rightAnchor),
            segmentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            widthAnchor.constraint(equalToConstant: 130),
        ])
    }
}
