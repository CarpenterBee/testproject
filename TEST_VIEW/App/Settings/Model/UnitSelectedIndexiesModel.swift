//
//  UnitSelectedIndexiesModel.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 18.11.2023.
//

import Foundation

enum SegmetType: String {
    case False
    case True
}

public struct UnitSelectedStatesModel: Codable {
        var height: Bool
        var diameter: Bool
        var mass: Bool
        var payload: Bool
        
        public init(height: Bool = false,
                    diameter: Bool = false,
                    mass: Bool = false,
                    payload: Bool = false) {
            self.height = height
            self.diameter = diameter
            self.mass = mass
            self.payload = payload
        }

    }

