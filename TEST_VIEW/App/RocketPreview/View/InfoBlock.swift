//
//  StepBlock.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 14.10.2023.
//

import Foundation
import UIKit

final class InfoBlock: UIView {
    private let infoContainer = UIStackView()
    private let unitsContainer = UIStackView()
    
    private let title = UILabel()
    private let firstInfo = UILabel()
    private let secondInfo = UILabel()
    private let thirdInfo = UILabel()
    
    private var type: BlockType
    
    public init(type: BlockType = .rocketInfo) {
        self.type = type
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateInfoLabels(first: String,
                                 second: String,
                                 third: String) {
        self.firstInfo.text = first
        self.secondInfo.text = second
        self.thirdInfo.text = third
    }
    
    public func updatePreviewLabel(title: String? = nil,
                                   first: String,
                                   second: String,
                                   third: String) {
        if let text = title {
            self.title.text = text
        }
        for (index, stack) in infoContainer.arrangedSubviews.enumerated() {
            guard let horizontalStack = stack as? UIStackView,
                  let label = horizontalStack.arrangedSubviews.first as? UILabel else { return }
            switch index {
            case 0: label.text = first
            case 1: label.text = second
            case 2: label.text = third
            default: break
            }
        }
    }
    
    private func setupUI() {
        setupTitle()
        setupContainer()
        setupTitlesInfo()
        switch type {
        case .rocketInfo: break
        case .step:
            setupUnitsContainer()
            setupUnitsInfo()
        }
    }
    
    private func setupTitle() {
        title.setContentHuggingPriority(.defaultHigh, for: .vertical)
        title.textAlignment = .left
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = .systemFont(ofSize: 25, weight: .heavy)
        title.textColor = .white
        self.addSubview(title)
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: self.topAnchor),
            title.leftAnchor.constraint(equalTo: self.leftAnchor),
            title.rightAnchor.constraint(equalTo: self.rightAnchor),
        ])
    }
    
    private func setupContainer() {
        infoContainer.backgroundColor = .black
        infoContainer.axis = .vertical
        infoContainer.distribution = .fillEqually
        infoContainer.spacing = 0
        infoContainer.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(infoContainer)
        NSLayoutConstraint.activate([
            infoContainer.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 22),
            infoContainer.leftAnchor.constraint(equalTo: self.leftAnchor),
            infoContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        if type == .rocketInfo {
            infoContainer.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        }
    }
    
    private func setupTitlesInfo() {
        infoContainer.addArrangedSubview(createOneInfo(info: .engines))
        infoContainer.addArrangedSubview(createOneInfo(info: .fuel))
        infoContainer.addArrangedSubview(createOneInfo(info: .burningTime))
    }
    
    private func setupUnitsContainer() {
        unitsContainer.backgroundColor = .black
        unitsContainer.axis = .vertical
        unitsContainer.distribution = .fillEqually
        unitsContainer.spacing = 3
        unitsContainer.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(unitsContainer)
        NSLayoutConstraint.activate([
            unitsContainer.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 20),
            unitsContainer.rightAnchor.constraint(equalTo: self.rightAnchor),
            unitsContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            unitsContainer.leftAnchor.constraint(equalTo: self.infoContainer.rightAnchor, constant: 10 ),
        ])
    }
    
    private func createOneInfo(info: StepInfo) -> UIStackView {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        
        let infoLabel = UILabel()
        infoLabel.textAlignment = .left
        
        let labelToInsert: UILabel
        infoLabel.textColor = .white
        switch info {
        case .engines:
            infoLabel.text = "Количество двигателей"
            labelToInsert = firstInfo
        case .fuel:
            infoLabel.text = "Количество топлива"
            labelToInsert = secondInfo
        case .burningTime:
            infoLabel.text = "Время сгорания"
            labelToInsert = thirdInfo
        }
        
        labelToInsert.textAlignment = .right
        labelToInsert.numberOfLines = 0
        labelToInsert.setContentHuggingPriority(.defaultLow, for: .horizontal)
        labelToInsert.text = "0"
        labelToInsert.font = .systemFont(ofSize: 18)
        labelToInsert.textColor = .white
        stack.addArrangedSubview(infoLabel)
        stack.addArrangedSubview(labelToInsert)
        
        return stack
    }
    
    private func setupUnitsInfo() {
        unitsContainer.axis = .vertical
        
        ["", "ton", "sec"].forEach { [weak self] unit in
            let label = UILabel()
            label.text = unit
            label.textColor = .white
            label.font = .systemFont(ofSize: 18)
            label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
            label.adjustsFontSizeToFitWidth = true
            self?.unitsContainer.addArrangedSubview(label)
        }
    }
}
