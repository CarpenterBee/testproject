//
//  CustomCollectionViewCell.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 20.09.2023.
//

import UIKit

final class CustomCollectionViewCell: UICollectionViewCell {
    private let title = UILabel()
    private let subtitle = UILabel()
    private let cellStackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateTexts(title titleStr: String, subtitle subtitleStr: String) {
        title.text = titleStr
        subtitle.text = subtitleStr
    }
    
    private func setupUI() {
        setupCell()
        setupTitle()
        setupSubtitle()
        setupCellStackView()
    }
    
    private func setupCellStackView() {
        contentView.addSubview(cellStackView)
        
        cellStackView.axis = .vertical
        cellStackView.distribution = .fillEqually
        cellStackView.spacing = 0
        cellStackView.addArrangedSubview(title)
        cellStackView.addArrangedSubview(subtitle)
       
        cellStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cellStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25),
            cellStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -25),
            cellStackView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            cellStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
    }
    
    private func setupCell() {
        contentView.backgroundColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1)
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 40
    }
    
    private func setupTitle() {
        title.text = "Custom"
        title.textAlignment = .center
        title.textColor = .white
        title.font = .systemFont(ofSize: 18, weight: .bold)
        
    }
    
    private func setupSubtitle() {
        subtitle.text = "Custom"
        subtitle.textColor = .lightGray
        subtitle.textAlignment = .center
        subtitle.font = .systemFont(ofSize: 16, weight: .medium)
    }
    
}
