//
//  RocketPreviewViewController.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 17.09.2023.
//
protocol ReloadCollection: AnyObject {
    func update()
}

import UIKit

public final class RocketPreviewViewController: UIViewController {
    private let scroll = UIScrollView()
    private let rocketImage = UIImageView()
    private let container = UIView()
    private let rocketName = UILabel()
    private let settingsButton = UIButton()
    private var infoCollection: UICollectionView!
    private let rocketInfoBlock = InfoBlock()
    private let firstStepInfo = InfoBlock(type: .step)
    private let secondStepInfo = InfoBlock(type: .step)
    private let launchesButton = UIButton()
    private var rocketLaunches = [LaunchModel]()
    private var infoCollectionData = RocketCollectionModel()
    private var infoData = [RocketInfoModel]() {
        didSet {
            self.infoCollection.reloadData()
        }
    }
    private var rocketData: RocketData
    private var launches: [LaunchData]
    
    public init(info: RocketData, launches: [LaunchData]) {
        self.rocketData = info
        self.launches = launches
        super.init(nibName: nil, bundle: nil)
        setupUI()
    }
    
    private func updateLaunch() {
        rocketLaunches = launches.map {
            let type = launchType(successLaunch: $0.success)
            return LaunchModel(title: $0.name, subtitle: $0.dateUTC, LaunchType: type)
        }
    }
    
    private func updateCollection() {
        infoCollectionData.MassKg = String(rocketData.mass.kg)
        infoCollectionData.MassLb = String(rocketData.mass.lb)
        infoCollectionData.diameterM = String(rocketData.diameter.meters ?? -404)
        infoCollectionData.diameterFt = String(rocketData.diameter.feet ?? -404)
        infoCollectionData.heightFt = String(rocketData.diameter.feet ?? 0)
        infoCollectionData.heightM = String(rocketData.height.meters ?? 0)
        infoCollectionData.payloadWeightsKg = String(rocketData.payloadWeights.first?.kg ?? 0)
        infoCollectionData.payloadWeightsLb = String(rocketData.payloadWeights.first?.lb ?? 0)
    }
    
    private func updateInfoBlock() {
        
        var formattedData: String {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-mm-dd"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"
            
            let date: Date? = dateFormatterGet.date(from: rocketData.firstFlight)
            
            return String(describing: dateFormatterPrint.string(from: date!))
        }
        
        let country: String
            if rocketData.country == "United States" {
                country  = "США"
            } else {
                country = "М. Острова"
            }
        
        rocketInfoBlock.updateInfoLabels(first: formattedData,
                                         second: country ,
                                         third: "$" + (rocketData.costPerLaunch/1_000_000).description + " млн")
        firstStepInfo.updateInfoLabels(first: String(rocketData.firstStage.engines),
                                       second: String(rocketData.firstStage.fuelAmountTons),
                                       third: String(rocketData.firstStage.burnTimeSEC ?? 0))
        secondStepInfo.updateInfoLabels(first: String(rocketData.secondStage.engines),
                                        second: String(rocketData.secondStage.fuelAmountTons),
                                        third: String(rocketData.secondStage.burnTimeSEC ?? 0))
    }
    
    private func launchType(successLaunch: Bool?) -> Launch {
        guard let successLaunch = successLaunch else { return .noLaunch }
        return successLaunch ? .success : .failure
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        infoCollection.delegate = self
        infoCollection.dataSource = self
        
        rocketInfoBlock.updatePreviewLabel(first: "Первый запуск",
                                               second: "Страна",
                                               third: "Стоимость запуска")
        firstStepInfo.updatePreviewLabel(title: "Первая ступень",
                                         first: "Количество двигателей",
                                         second: "Количество топлива",
                                         third: "Время сгорания")
        secondStepInfo.updatePreviewLabel(title: "Вторая ступень",
                                          first: "Количество двигателей",
                                          second: "Количество топлива",
                                          third: "Время сгорания")
    }
    public override func viewWillAppear(_ animated: Bool) {
        infoCollection.reloadData()
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        print(infoCollectionData)
        updateInfoCollection()
    }
    
    public func updateRocketName(taitalName:String) {
        self.rocketName.text = taitalName
    }
    
    private func updateInfoCollection(){
        infoData.removeAll()
        
                if let segmentData = UserDefaults.standard.object(forKey: "segmentData") as? Data {
                    let decoder = JSONDecoder()
                    if let loadedSegmentData = try? decoder.decode(UnitSelectedStatesModel.self, from: segmentData){
        
                        let mass = loadedSegmentData.mass ?  infoCollectionData.MassLb : infoCollectionData.MassKg
                        let massLable = loadedSegmentData.mass ? "масс, lb" : "масс, kg"
                        let diameter = loadedSegmentData.diameter ? infoCollectionData.diameterFt : infoCollectionData.diameterM
                        let diameterLable = loadedSegmentData.diameter ? "диаметр, ft" : "диаметр, m"
                        let height = loadedSegmentData.height ? infoCollectionData.heightFt : infoCollectionData.heightM
                        let heightLable = loadedSegmentData.height ?  "выcoта, ft" : "выcoта, m"
                        let payloadWeights = loadedSegmentData.payload ? infoCollectionData.payloadWeightsLb : infoCollectionData.payloadWeightsKg
                        let payloadWeightsLable = loadedSegmentData.payload ? "вес, lb" : "вес, kg"
                                            
                        infoData.append(contentsOf: [RocketInfoModel(title: height, subtitle: heightLable),
                                                     RocketInfoModel(title: diameter, subtitle: diameterLable),
                                                     RocketInfoModel(title: mass, subtitle: massLable),
                                                     RocketInfoModel(title: payloadWeights, subtitle: payloadWeightsLable)
                                                    ])
                    }
            }
    }
    
    private func setupUI() {
        setupContainer()
        setupRocketNameLabel()
        setupCollectionView()
        setupImageView()
        setuprocketInfoBlock()
        setupSettingsButton()
        setupStageferstRocketContainer()
        setupStageSecondRocketContainer()
        setupScroll()
        setupShowLaunchesButton()
        updateLaunch()
        updateCollection()
        updateInfoBlock()
    }
    
    private func setupScroll() {
        scroll.contentInsetAdjustmentBehavior = .never
        scroll.backgroundColor = .black
        scroll.isDirectionalLockEnabled = true

        scroll.delegate = self
        view.addSubview(scroll)
        scroll.showsVerticalScrollIndicator = false

        scroll.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scroll.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scroll.leftAnchor.constraint(equalTo: view.leftAnchor),
            scroll.rightAnchor.constraint(equalTo: view.rightAnchor),
            scroll.topAnchor.constraint(equalTo: view.topAnchor),
        ])
    }
    
    private func setupContainer() {
        scroll.addSubview(container)
        container.backgroundColor = .black
        
        container.layer.cornerRadius = 50
        container.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.bottomAnchor.constraint(equalTo: scroll.bottomAnchor),
            container.leftAnchor.constraint(equalTo: scroll.leftAnchor),
            container.rightAnchor.constraint(equalTo: scroll.rightAnchor),
            container.topAnchor.constraint(equalTo: scroll.topAnchor, constant: 466),
        ])
    }
    
    private func setupCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
        layout.itemSize = CGSize(width: 110, height: 110)
        layout.scrollDirection = .horizontal
        
        infoCollection = UICollectionView(frame: .zero, collectionViewLayout: layout)

        infoCollection.backgroundColor = .black
        container.addSubview(infoCollection)
        infoCollection.showsHorizontalScrollIndicator = false
        
        infoCollection.delegate = self
        infoCollection.dataSource = self
        infoCollection.register(CustomCollectionViewCell.self,
                                forCellWithReuseIdentifier: String(describing: CustomCollectionViewCell.self))
        
        infoCollection.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            infoCollection.leftAnchor.constraint(equalTo: container.leftAnchor),
            infoCollection.rightAnchor.constraint(equalTo: container.rightAnchor),
            infoCollection.topAnchor.constraint(equalTo: rocketName.bottomAnchor, constant: 20),
            infoCollection.heightAnchor.constraint(equalToConstant: 110),
            infoCollection.widthAnchor.constraint(equalTo: scroll.widthAnchor)
        ])
    }
    
    private func setupImageView() {
        loadImage(url: rocketData.flickrImages.randomElement())
        scroll.addSubview(rocketImage)
        scroll.sendSubviewToBack(rocketImage)
        
        rocketImage.backgroundColor = .brown
        rocketImage.contentMode = .scaleToFill
        
        rocketImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rocketImage.topAnchor.constraint(equalTo: scroll.topAnchor, constant: -50),
            rocketImage.rightAnchor.constraint(equalTo: scroll.rightAnchor,constant: 30),
            rocketImage.bottomAnchor.constraint(equalTo: container.topAnchor, constant: 50),
            rocketImage.leftAnchor.constraint(equalTo: scroll.leftAnchor, constant: -30),
        ])
    }
    
    private func setupSettingsButton() {
        settingsButton.backgroundColor = .black
        let configuration = UIImage.SymbolConfiguration(pointSize: 26)
        settingsButton.setImage(UIImage(systemName: "gearshape",
                                        withConfiguration: configuration),
                                for: .normal)
        settingsButton.tintColor = .white
        settingsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        settingsButton.addTarget(self, action: #selector(showSettingsScreen),
                           for: .touchUpInside)
        
        container.addSubview(settingsButton)
        
        settingsButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            settingsButton.bottomAnchor.constraint(equalTo: rocketName.bottomAnchor, constant: 0),
            settingsButton.rightAnchor.constraint(equalTo: container.rightAnchor,constant: -32),
            settingsButton.heightAnchor.constraint(equalToConstant: 50),
            settingsButton.widthAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    private func setuprocketInfoBlock() {
        container.addSubview(rocketInfoBlock)
        rocketInfoBlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rocketInfoBlock.topAnchor.constraint(equalTo: infoCollection.bottomAnchor, constant: 20),
            rocketInfoBlock.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 25),
            rocketInfoBlock.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -25),
            rocketInfoBlock.heightAnchor.constraint(equalToConstant: 190),
        ])
    }
    
    private func setupStageferstRocketContainer() {
        container.addSubview(firstStepInfo)
        firstStepInfo.backgroundColor = .black
        firstStepInfo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            firstStepInfo.topAnchor.constraint(equalTo: rocketInfoBlock.bottomAnchor, constant: 40),
            firstStepInfo.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 25),
            firstStepInfo.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -25),
            firstStepInfo.heightAnchor.constraint(equalToConstant: 190),
        ])
    }
    
    private func setupStageSecondRocketContainer() {
        container.addSubview(secondStepInfo)
        secondStepInfo.backgroundColor = .black
        secondStepInfo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            secondStepInfo.topAnchor.constraint(equalTo: firstStepInfo.bottomAnchor, constant: 40),
            secondStepInfo.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 25),
            secondStepInfo.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -25),
            secondStepInfo.heightAnchor.constraint(equalToConstant: 190),
        ])
    }
    
    public func setupRocketNameLabel() {
        rocketName.text = rocketData.name
        rocketName.textAlignment = .left
        rocketName.font = .systemFont(ofSize: 32, weight: .medium)
        rocketName.textColor = .white
        
        container.addSubview(rocketName)
        
        rocketName.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rocketName.topAnchor.constraint(equalTo: container.topAnchor, constant: 50),
            rocketName.leftAnchor.constraint(equalTo: container.leftAnchor,constant: 32),
        ])
    }
    
    public func loadImage(url: String?) {
        guard let urlStr = url,
              let apiURL = URL(string: urlStr) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: apiURL) { data, _, _ in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.rocketImage.image = UIImage(data: data)
            }
        }
        task.resume()
    }

    @objc func showSettingsScreen() {
        let viewController = SettingsViewController()
        viewController.RreloadCollectionreloadCollection = self
        self.present(viewController, animated: true)
    }
    
    @objc private func ShowLaunches() {
        let launchVC = LaunchDataViewController(title: rocketData.name)
        launchVC.setup(launches: rocketLaunches)
        navigationController?.pushViewController(launchVC, animated: true)
    }
    
    private func setupShowLaunchesButton() {
        launchesButton.backgroundColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1)
        launchesButton.setTitle("Посмотреть запуски", for: .normal)
        container.addSubview(launchesButton)
        launchesButton.layer.cornerRadius = 25
        launchesButton.translatesAutoresizingMaskIntoConstraints = false
        
        launchesButton.addTarget(self, action: #selector(ShowLaunches),
                           for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            launchesButton.topAnchor.constraint(equalTo: secondStepInfo.bottomAnchor, constant: 30),
            launchesButton.rightAnchor.constraint(equalTo: container.rightAnchor,constant: -35),
            launchesButton.leftAnchor.constraint(equalTo: container.leftAnchor,constant: 35),
            launchesButton.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -60),
            launchesButton.heightAnchor.constraint(equalToConstant: 70)
        ])
    }

}

extension RocketPreviewViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return infoData.count
    }
        
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CustomCollectionViewCell.self), for: indexPath) as? CustomCollectionViewCell else {return UICollectionViewCell()}
        
        let titleName = infoData[indexPath.item].title
        let subtitleName = infoData[indexPath.item].subtitle
        
        cell.updateTexts(title: titleName, subtitle: subtitleName)
        
        return cell
    }
}

extension RocketPreviewViewController: ReloadCollection {
    func update() {
        self.updateInfoCollection()
    }
}
