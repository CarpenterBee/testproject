//
//  RocketInfoModel.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 17.11.2023.
//

import Foundation

public struct RocketInfoModel {
    var title: String
    var subtitle: String
}

public struct RocketCollectionModel {
    var MassKg: String
    var MassLb: String
    var diameterM: String
    var diameterFt: String
    var heightM: String
    var heightFt: String
    var payloadWeightsKg: String
    var payloadWeightsLb: String
    
    public init( MassKg: String = "",
                 MassLb: String = "",
                 diameterM: String = "",
                 diameterFt: String = "",
                 heightM: String = "",
                 heightFt: String = "",
                 payloadWeightsKg: String = "",
                 payloadWeightsLb: String = "") {
        self.MassKg = MassKg
        self.MassLb = MassLb
        self.diameterM = diameterM
        self.diameterFt = diameterFt
        self.heightM = heightM
        self.heightFt = heightFt
        self.payloadWeightsKg = payloadWeightsKg
        self.payloadWeightsLb = payloadWeightsLb
    }
}
