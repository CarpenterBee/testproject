//
//  PageRocketController.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 05.11.2023.
//

import UIKit

public final class PageRocketController: UIPageViewController {
    private let pageControl = UIPageControl()
    private let scroll = UIScrollView()
    private var rocketsInfo: [RocketData] = []
    private var allRocketLaunches: [LaunchData] = []
    
    lazy var arrayViewController: [RocketPreviewViewController] = {
        var rocketVC: [RocketPreviewViewController] = []
        for (rocketIndex, rocketInfo) in self.rocketsInfo.enumerated() {
            let rocketId = rocketInfo.id
            let rocketLaunches = self.allRocketLaunches.compactMap {
                $0.rocket == rocketId ? $0 : nil
            }
            rocketVC.append(RocketPreviewViewController(info: rocketInfo, launches: rocketLaunches))
        }
        return rocketVC
    }()
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        setupAllData()
    }
    
    private func setupAllData() {
        let group = DispatchGroup()
        
        group.enter()
        getRocketInfoJSON { rocketData in
            self.rocketsInfo = rocketData ?? []
            group.leave()
        }
        
        group.enter()
        getLaunchDataJSON { launchData in
            self.allRocketLaunches = launchData ?? []
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.setViewControllers([self.arrayViewController[0]],
                                    direction: .forward,
                                    animated: true)
        }
    }
}

extension PageRocketController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return rocketsInfo.count
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? RocketPreviewViewController else {return nil}
        if let index = arrayViewController.firstIndex(of: vc),
           index > 0 {
            return arrayViewController[index - 1]
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? RocketPreviewViewController else {return nil}
        if let index = arrayViewController.firstIndex(of: vc),
           index < arrayViewController.count - 1 {
            return arrayViewController[index + 1]
        }
        return nil
    }
}
