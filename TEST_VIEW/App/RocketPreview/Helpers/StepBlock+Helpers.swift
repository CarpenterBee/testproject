//
//  StepBlock+Helpers.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 15.10.2023.
//

import UIKit

public enum StepInfo: CaseIterable {
    case engines
    case fuel
    case burningTime
}

public enum BlockType {
    case rocketInfo
    case step
}
