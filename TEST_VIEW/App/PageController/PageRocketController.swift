//
//  PageRocketController.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 05.11.2023.
//

import UIKit

class PageRocketController: UIPageViewController {
    private let pageControl = UIPageControl()
    private let scroll = UIScrollView()
    private var rocketsInfo: [RocketData] = []
    private var allRocketLaunches: [LaunchData] = []
    
    lazy var arrayViewController: [ViewController] = {
        var rocketVC: [ViewController] = []
        for (rocketIndex, rocketInfo) in self.rocketsInfo.enumerated() {
            let rocketId = rocketInfo.id
            let rocketLaunches = self.allRocketLaunches.compactMap {
                if $0.rocket == rocketId {
                    return $0
                } else { return nil }
            }
            rocketVC.append(ViewController(info: rocketInfo, launches: rocketLaunches))
        }
        return rocketVC
    }()
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation)
        //        self.setViewControllers([self.arrayViewController[0]],
        //                                direction: .forward,
        //                                animated: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        //    override func viewWillAppear(_ animated: Bool) {
        //        super.viewWillAppear(animated)
        
        doAllSheetWeNeed()
        
        
    }
    
    private func doAllSheetWeNeed() {
        let group = DispatchGroup()
        
        group.enter()
        getRocketInfoJSON { rocketData in
            self.rocketsInfo = rocketData ?? []
            group.leave()
        }
        
        group.enter()
        getLaunchDataJSON { launchData in
            self.allRocketLaunches = launchData ?? []
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.setViewControllers([self.arrayViewController[0]],
                                    direction: .forward,
                                    animated: true)
        }
    }
}

extension PageRocketController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? ViewController else {return nil}
        if let index = arrayViewController.index(of: vc),
           index > 0 {
            return arrayViewController[index - 1]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? ViewController else {return nil}
        if let index = arrayViewController.index(of: vc),
           index < arrayViewController.count - 1 {
            return arrayViewController[index + 1]
        }
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return rocketsInfo.count

    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
