//
//  RocketParameters.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 03.10.2023.
//

import Foundation

public struct Parameters {
    var title: String
    var subtitle: String
}
public enum Launch {
    case success
    case failure
    case noLaunch
}

public struct DataFlight {
    var title: String
    var subtitle: String
    var LaunchType: Launch
}
