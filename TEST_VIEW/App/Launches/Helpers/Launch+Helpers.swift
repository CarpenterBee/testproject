//
//  Helpers.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 17.11.2023.
//

import Foundation

public enum Launch {
    case success
    case failure
    case noLaunch
}
