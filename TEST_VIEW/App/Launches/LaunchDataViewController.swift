//
//  LaunchDataViewController.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 18.10.2023.
//

import UIKit

public final class LaunchDataViewController: UIViewController {
    
    private var infoFlightData: UICollectionView!
    private var launches = [LaunchModel]()
    private var launchName: String
    
    public init(title: String) {
        self.launchName = title
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        self.navigationItem.titleView?.tintColor = .white
        setupCollectionView()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.backgroundColor = .black
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.title = launchName
    }
    
    public func setup(launches: [LaunchModel]) {
        self.launches = launches
    }
    
    private func setupCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 310, height: 100)
        layout.scrollDirection = .vertical
        
        infoFlightData = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        infoFlightData.backgroundColor = .black
        view.addSubview(infoFlightData)
        
        infoFlightData.delegate = self
        infoFlightData.dataSource = self
        infoFlightData.register(LaunchCell.self,
                                forCellWithReuseIdentifier: String(describing: LaunchCell.self))
        
        infoFlightData.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            infoFlightData.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 25),
            infoFlightData.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -25),
            infoFlightData.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            infoFlightData.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
        ])
    }
    
}

extension LaunchDataViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return launches.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LaunchCell.self), for: indexPath) as? LaunchCell else { return UICollectionViewCell() }
        
        let titleName = launches[indexPath.item].title
        let subtitleName = launches[indexPath.item].subtitle
        
        var formattedData: String {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"
            
            let date: Date? = dateFormatterGet.date(from: subtitleName)
            
            return String(describing: dateFormatterPrint.string(from: date!))
        }
        

        let launchResult = launches[indexPath.item].LaunchType
        switch launchResult {
            case .success:
                cell.setupLaunch(type: .success)
            case .failure:
                cell.setupLaunch(type: .failure)
            case .noLaunch:
                cell.setupLaunch(type: .noLaunch)
            
        }
        cell.updateFlyInfo(title: titleName, subtitle: formattedData)
        
        return cell
    }
}

