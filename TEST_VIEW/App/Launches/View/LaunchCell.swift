//
//  LaunchCell.swift
//  TEST_VIEW
//
//  Created by Роман Степанов on 20.10.2023.
//

import UIKit

final class LaunchCell: UICollectionViewCell {
    private let title = UILabel()
    private let subtitle = UILabel()
    private let cellStackView = UIStackView()
    private let rocketImage = UIImageView()
    
    public enum Launch {
        case success
        case failure
        case noLaunch
    }

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupSubtitle()
        setupCellStackView()
        setupCell()
        setupTitle()
        setupUnitsContainer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupCellStackView() {
        contentView.addSubview(cellStackView)
        cellStackView.backgroundColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1)
        
        cellStackView.axis = .vertical
        cellStackView.distribution = .fillEqually
        cellStackView.spacing = 0
        cellStackView.addArrangedSubview(title)
        cellStackView.addArrangedSubview(subtitle)
        
        cellStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cellStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            cellStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
            cellStackView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 30),
            cellStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -100),
        ])
    }
    
    private func setupCell() {
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 30
        contentView.backgroundColor = UIColor(red: 0.08, green: 0.08, blue: 0.08, alpha: 1)
    }
    
    private func setupTitle() {
        title.text = "FalconSat"
        title.textColor = .white
        title.textAlignment = .left
        title.font = .systemFont(ofSize: 20, weight: .bold)
        
    }
    
    private func setupSubtitle() {
        subtitle.text = "2 февраля, 2022"
        subtitle.textColor = .lightGray
        subtitle.textAlignment = .left
        subtitle.font = .systemFont(ofSize: 18, weight: .light)
    }
    
    private func setupUnitsContainer() {
        rocketImage.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(rocketImage)
        NSLayoutConstraint.activate([
            rocketImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 20 ),
            rocketImage.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -30),
            rocketImage.heightAnchor.constraint(equalToConstant: 60),
            rocketImage.widthAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    public func setupLaunch(type:  Launch) {
        let imageName: String
        switch type {
        case .success:
           imageName = "Success"
        case .failure:
            imageName = "Failure"
        case .noLaunch:
            imageName = "NoLaunch"
        }
        rocketImage.image = UIImage.init(named: imageName)
    }
    public func updateFlyInfo(title titleFly: String, subtitle subtitleFly: String) {
        title.text = titleFly
        subtitle.text = subtitleFly
        }
    }
