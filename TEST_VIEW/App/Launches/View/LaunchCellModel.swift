//
//  File.swift
//  TEST_VIEW
//
//  Created by Роман Хоменко on 21.10.2023.
//

import Foundation
import UIKit

struct LaunchCellModel {
    let name: String
    let launchDate: String
    let statusImage: UIImage
}
